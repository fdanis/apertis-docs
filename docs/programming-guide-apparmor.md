---
short-description: "Use AppArmor add a security layer which enforces access control"

authors:
    - name: Ekaterina Gerasimova
      email: kat@collabora.co.uk
      years: [2016]
    - name: Philip Withnall
      email: philip@tecnocode.co.uk
      years: [2015]

license: CC-BY-SAv4.0
...

# Security and access control

AppArmor is a security layer which enforces access control on the filesystem resources applications can access, and the permissions they can access them with. It comprises a kernel module and user space profiles for each application, which define the resources an application expects to access. For more information, see the [AppArmor home page](http://wiki.apparmor.net/index.php/Main_Page). Apertis uses AppArmor for all applications and services.

## Summary

* [Write AppArmor profiles](#profiles) to be as constrained as possible.
* [Validate the profiles](#validation) manually before release, and during testing.

## Profiles

For application development, the only work which needs to be done for AppArmor integration is to write and install a profile for the application. See the [AppArmor website](http://wiki.apparmor.net/index.php/Profiles) for information on writing profiles. Profiles should be as constrained as possible, following the [principle of least privilege](http://en.wikipedia.org/wiki/Principle_of_least_privilege).

To install a profile, use the following `Makefile.am` snippet:
```
aaprofiledir = $(sysconfdir)/apparmor.d
aaprofile_DATA = usr.bin.application-name
EXTRA_DIST = $(aaprofile_DATA)
```

## Validation

AppArmor profiles can be validated in two ways: manually, and at runtime. Manual verification should be performed before each release, manually inspecting the profile against the list of changes made to the application since the last release, and checking that each entry is still relevant and correct, and that no new entries are needed.

Runtime verification is automatic: AppArmor will deny access to files which violate the profile, emitting a message in the audit logs (`audit.log`), for example:
```
Feb 23 18:54:07 my-host kernel: [   24.610703] type=1400 audit(1393181647.872:15): apparmor="DENIED" operation="open" parent=1 profile="/usr/sbin/ntpd" name="/etc/ldap/ldap.conf" pid=1526 comm="ntpd" requested_mask="r" denied_mask="r" fsuid=0 ouid=0
```

Such messages should be investigated, and may result in changes to the application (to prevent it making such accesses) or to the profile (to allow them).

Manual and runtime verification are complementary: manual verification ensures the profile is as small as possible; runtime verification ensures the profile is as big as it needs to be.

## External links

* [AppArmor home page](http://wiki.apparmor.net/index.php/Main_Page)
* [AppArmor profile wiki page](http://wiki.apparmor.net/index.php/Profiles)
