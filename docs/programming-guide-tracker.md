---
short-description: "Use Tracker to index file metadata and search storage devices"

authors:
    - name: Ekaterina Gerasimova
      email: kat@collabora.co.uk
      years: [2016]
    - name: Philip Withnall
      email: philip@tecnocode.co.uk
      years: [2015]

license: CC-BY-SAv4.0
...

# Indexing and searching

[Tracker](https://wiki.gnome.org/Projects/Tracker) is a desktop search engine, metadata indexing and storage service. It is the recommended way to search for user files and access metadata about them. A [full introduction to Tracker is here](https://wiki.gnome.org/Projects/Tracker/WhatIsTracker).

## Summary

* [Avoid SPARQL injection vulnerabilities](#sparql-injection) by using prepared statements.

## Using Tracker

Tracker is effectively a metadata store which applications can query using the [SPARQL query language](http://en.wikipedia.org/wiki/SPARQL). SPARQL is similar to SQL, so all the same considerations about [SQL injection](#sparql-injection) apply when using it.

Describing how to use Tracker is beyond the scope of this document, however it has good documentation in its [getting started guide](https://wiki.gnome.org/Projects/Tracker/Documentation/GettingStarted) and [API documentation](https://wiki.gnome.org/Projects/Tracker/Documentation).

## SPARQL injection

When using Tracker, queries '''must''' be constructed using its prepared statements, otherwise arbitrary SPARQL could be provided by the user which would affect the query, potentially resulting in unauthorised user data disclosure. This would be an [SQL injection vulnerability](http://en.wikipedia.org/wiki/SQL_injection).

To build a SPARQL query, use [`TrackerSparqlBuilder`](https://developer.gnome.org/libtracker-sparql/stable/TrackerSparqlBuilder.html), which prevents SPARQL injection vulnerabilities as long as its ‘raw’ APIs aren’t used. If its raw APIs are used, be very careful to escape all external input to the query using [`tracker_sparql_escape_string()`](https://developer.gnome.org/libtracker-sparql/stable/libtracker-sparql-Utilities.html#tracker-sparql-escape-string) before including it in the query.

## External links

* [Tracker home page](https://wiki.gnome.org/Projects/Tracker)
* [Introduction to Tracker](https://wiki.gnome.org/Projects/Tracker/WhatIsTracker)
* [SPARQL query language](http://en.wikipedia.org/wiki/SPARQL)
* [Tracker getting started guide](https://wiki.gnome.org/Projects/Tracker/Documentation/GettingStarted)
* [Tracker API documentation](https://wiki.gnome.org/Projects/Tracker/Documentation)

