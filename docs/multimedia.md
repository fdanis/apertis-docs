---
short-description: "Multimedia interfaces provided by the SDK"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Multimedia

### Provides:

- Playback of audio and video contents (local and streaming)

- Capturing images and recording audio and video

- Scanning & Playback of radio

- Complete audio management

- Extracting and displaying media content information

- Integration of External apps like VLC.

### Enabling components:

- GStreamer: Audio, Video, Recording, Streaming, Etc

- Audio Manager: Audio Policy Management

- PulseAudio: Software mixing multiple audio streams

- Multiple-Format Codec: Various support of codec

- Media Content Service: Content management for media files

### SDK services:

- libgrassmoor : Libgrassmoor is a library responsible for providing
  media info and media playback functionalities

- Tinwell: Is our Audio service. It focuses on complete audio handling
  for the system and provides facilities to play, buffer, record audio
  streams. It interacts with Canterbury to manage system audio
  effectively when multiple audio sources are active.

- Prestwood: This service manages media and handles any activity
  related to the filesystem (mounted), UPnP devices etc and indicates
  when they are ready for other services to communicate with them.
