---
short-description: "The application framework"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Application framework

### Provides:

- Launching a new Application (Launch-mgr)

- Explicit or implicit information (Combination of Action, URI, and
  MIME) can be used to determine an app to launch

- Application life cycle management (activity-mgr)

- Managing application launch history (LUM)

- Audio management and audio priority handling(audio-manager)

- Sharing mechanism

- Application preferences (prefernce-mgr)

- IPC communication (D-Bus)

- View switching and in app back handling (View-Manager)

### SDK services:

- Canterbury: This is the core process in the system which deals with
  launch and kill of applications to be run on the system, including
  management of applications life cycle, creation and maintenance of
  application stack, etc. It also interacts on a regular basis with
  few other services for resource management of applications’.
  Interaction with audio service (for audio handling and management),
  data exchange service (to facilitate inter application data
  exchange) are 2 examples.

- Didcot: This service enables data sharing between applications
  depending on mime type of the data to be shared.

- Newport: It handles all downloads running in the system (from
  browser, email, other devices etc).

- Barkway: This service caters to the global UI layer like popup
  available in the system.

- Chalgrove: It is the backend data service to manage per user or even
  system settings. It interacts with canterbury to store some app
  related settings.

- libseaton: it provides interfaces to store persistent data. It is a
  shared library using SQLite as the backend database.

- Libclapton: LibClapton is a library which is used for logging.
