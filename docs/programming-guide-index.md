---
authors:
    - name: Ekaterina Gerasimova
      email: kat@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Programming guidelines

This guide contains several guidelines and suggestions for programmers developing applications for the Apertis platform. This is intended for programmers to know about the processes, conventions and philosophies behind Apertis and the stack of libraries supporting it. By knowing “the way things are done” in the Apertis ecosystem, it is hoped that programmers will find use of Apertis APIs and development of new applications easier and more natural, and will produce applications which will remain relevant and maintainable over a long period of time.
