---
short-description: "Wi-Fi, Bluetooth ..."

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Connectivity

### Provides:

- Cellular and Wi-Fi Connection
  
    - Connman manages internet connections (Automatically connects to
      last used network).

- Bluetooth
  
    - Based on Bluez and profiles (PBAP, A2DP, PAN)
  
    - Discovering / bonding / getting contacts from cell phone

### Enabling components:

- libfolks-eds: library that aggregates contacts from multiple sources
  with Evolution-data-server as backend
  (https://wiki.gnome.org/Projects/Folks)

### SDK services

- Beckfoot: Beckfoot is performs connection management and handles
  system connectivity services like network handling, wifi management,
  management of Bluetooth devices etc.

- Corbridge: It daemon for managing bluetooth functionalities like
  pairing, unpairing, A2DP connect/disconnect. And also contact Sync
  via bluetooth also implemented in this service.
