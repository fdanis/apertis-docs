---
short-description: "Collection of guides for using the SDK"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]
    - name: Peter Senna Tschudin
      email: peter.senna@collabora.co.uk
      years: [2019]

license: CC-BY-SAv4.0
...

# Using the SDK

## Overview of SDK features
Take a moment to familiarize yourself with some of the more commonly used tools in the SDK.  These include the file browser, Eclipse, DevHelp and the simulator.  All of these tools have shortcut links placed on the desktop.

## File browser
![](media/overview-home.png)
This icon opens the file manager so you can access the local folders and use the shared folder for exchanging documents between your host system and the virtual machine. You should be able to find your shared folder under **Places** on the left hand side of the file manager window.

## Eclipse IDE
![](media/overview-eclipse.png)
Eclipse is the IDE used for application development in the SDK. Eclipse includes a custom Apertis plugin for updating the sysroot from within Eclipse.

## Apertis API documentation
![](media/overview-docs.png)
DevHelp is the standard browser for offline API documentation across many Open Source projects. The DevHelp installation in the SDK provides a local, offline version of the all the documentation found in the Application Developer Portal, including the Apertis API references.

---

# Persistent workspace

The use of persistent workspace is optional. Check if you need persistent workspace before following these steps.

The SDK is distributed as VirtualBox images, and there is one VirtualBox image for each version of the SDK. The SDK includes a tool named psdk to help developers to migrate their data when upgrading to a newer version of the SDK. The basic idea is to use a second and persistent disk to store:
 - /home
 - /opt
 - /etc/cntlm.conf
 - Other configuration files selected by the developer

See the [design](https://designs.apertis.org/private/latest/maintaining-workspace-across-sdk-updates.html) notes for background information and [README](https://gitlab.apertis.org/apertis/psdk/blob/master/README.md) for the complete documentation of psdk.

> **WARNING**: It is recommended to make a backup copy of your SDK before continuing.

> **WARNING**: Configure the persistent disk on the **new** SDK before adding or changing files on the **new** SDK. Changes made to /home/user, /opt, and to /etc/cntlm.conf before configuring the persistent disk will be overwritten by the persistent disk. See the [README](https://gitlab.apertis.org/apertis/psdk/tree/apertis/v2020dev0#where-are-the-old-files-and-folders) for more details.

## Upgrading to a new SDK using a persistent disk

This involves two different SDK virtual machines: the *old* SDK and the *new* SDK. The suggested use is to first initialize the persistent disk on the *old* SDK, and then use the persistent disk on the *new* SDK.

## VirtualBox: Adding the persistent disk to the *old* SDK

Start by adding a second disk to the *old* SDK on VirtualBox. This disk should be big enough to host the contents of /home and /opt: we recommend using more than 40GiB and no less than 20GiB. We suggest using dynamically allocated VDI images so that the unused space does not consume disk space on the host.

Open the *Settings* window of the *old* SDK, click on the *Storage* tab, select *Controller SATA*, and click on the icon *Adds hard disk*:
![](media/VirtualBox-add-disk-1.png)

Choose *Create new disk*:
![](media/VirtualBox-add-disk-2.png)

Choose *VDI (VirtualBox Disk Image)* and click *Next*:
![](media/VirtualBox-add-disk-3.png)

Choose *Dynamically allocated* and click *Next*:
![](media/VirtualBox-add-disk-4.png)

Choose a folder to store the disk, a file name, and the size of the disk. Then click on *create*:
![](media/VirtualBox-add-disk-5.png)

## Preparing the persistent disk on the *old* SDK

psdk is installed by default on *base SDK* and on *SDK* images since Apertis v2019pre. The psdk package is also available on Apertis package repositories. If you want to install psdk on an older SDK, you can try installing the [Debian package](https://repositories.apertis.org/apertis/pool/sdk/p/psdk/) directly or install from the [source code](https://gitlab.apertis.org/apertis/psdk). At the time of writing psdk required the following packages to be installed: python3, zenity

After adding the second disk, start the *old* SDK and click on the psdk icon: Applications -> System -> *Persistent Disk Setup*.
![](media/psdk-icon.png)

psdk will open in interactive mode, and will guide you trough the processes of initializing the new disk and configuring the SDK to use it. Do not worry if the Linux device node(/dev/vdb) is different for you.
![](media/psdk-empty-found.png)

psdk will show progress while initializing the disk.
![](media/psdk-progress.png)

After psdk is done, it will ask you to reboot the SDK. Click OK, wait for the reboot to complete.
![](media/psdk-init-success.png)

After the reboot completes **turn off** the *old* SDK.

## VirtualBox: Adding the persistent disk to the *new* SDK

Open the *Settings* window of the *new* SDK, click on the *Storage* tab, select *Controller SATA*, and click on the icon *Adds hard disk*:
![](media/VirtualBox-add-disk-1.png)

Click on *Choose existing disk*:
![](media/VirtualBox-add-disk-2a.png)

The persistent disk will be under the category *Attached*. Select the persistent disk, and click on *Choose*:
![](media/VirtualBox-add-disk-3a.png)

## Using the persistent disk on the *new* SDK

The final step is to call psdk from the *new* SDK. Start the *new* SDK and click on the psdk icon: Applications -> System -> *Persistent Disk Setup*.
![](media/psdk-icon.png)

psdk will open in interactive mode, and will guide you trough the processes of configuring the *new* SDK to use the persistent disk. Do not worry if the Linux device node(/dev/vdb) is different for you.
![](media/psdk-found-persistent.png)

This will be much faster than initializing the disk. After psdk is done, it will ask you to reboot the SDK. Click OK, and wait for the reboot to complete.
![](media/psdk-conf-success.png)

---

Congratulations!  You now have everything you need to start developing applications for Apertis.

### Next

|-|
| ![](media/continue-arrow.png) **Start developing your app in the SDK.** [Continue on to application development](app-dev.md) |
