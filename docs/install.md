---
short-description: "Step-by-step guide for installing our SDK"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Installing the SDK

The SDK is a linux virtual machine running a standard desktop environment on top of the Apertis distribution.  It contains Apertis development libraries, developer tools, documentation, debugging libraries and the Apertis simulator.  The SDK VM image can be run on Windows, Mac OS, or different Linux distributions using the virtualization software VirtualBox.  Once you have verified that your system meets the [system requirements](#system-requirements) below, you'll need to [download an SDK VM image](#download-an-sdk-vm-image), and then [set up VirtualBox](vm-setup.md).

## System requirements

You will need a PC with the following configuration to install and run the SDK:

### Hardware

* Dual core CPU at 2GHz or higher

* 8 GB RAM or more

* 12 GB or more free space on the hard disk

* If your PC supports Virtualization Technology, make sure that it is enabled (if problems are seen in booting the SDK image, check if Virtualization Technology is enabled in BIOS settings)

### Software

* Windows/Linux/Mac OS

* Current installation of Oracle *VirtualBox*, version 5.0.12 or above (installed in next step)

## Download an SDK VM image
Apertis has a release lifecycle which is roughly three months long. Each release consists of a round of development, testing and bug fixing, followed by a stable release. Apertis also has a daily set of images which can be used for the latest testing.  Application developers should develop against the latest stable release image unless there is a good reason not to.  The latest stable release of Apertis is listed [on the wiki](https://wiki.apertis.org/ReleaseNotes).

* Stable release images are availble at [https://images.apertis.org/release/](https://images.apertis.org/release/)
* Daily build images are available at [https://images.apertis.org/daily/](https://images.apertis.org/daily/)

Download the gzipped VDI image of the latest stable release to your development PC.

### Next

|-|
| ![](media/continue-arrow.png) **Ready to install the downloaded SDK?** [Continue on to setting up the virtual machine](vm-setup.md) |
