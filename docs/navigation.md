---
short-description: "Navigation services (maps, guidance, ...)"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Navigation

### Provides:

- Location based services

- Map

- Route guidance

### Enabling components:

- Geoclue – Deliver location info from various positioning sources
  (http://freedesktop.org/wiki/Software/GeoClue/)

- Libchamplain – Map rendering
  (https://wiki.gnome.org/Projects/libchamplain)

- Younavigation – route drawing on map drawn by libchamplain
  (http://www.yournavigation.org/)
