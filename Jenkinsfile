def release = "v2020pre"

pipeline {
  options {
    timeout(time: 20, unit: 'MINUTES')
  }
  environment {
    RELEASE = "${release}"
  }
  agent {
    docker {
      label 'docker-slave'
      image "apertis/apertis-${release}-documentation-builder"
      registryUrl 'https://docker-registry.apertis.org'
    }
  }
  stages {
    stage("Build") {
      steps {
        sh "make"
        sh "DESTDIR=out make install"
      }
    }
    stage ("Upload site") {
      environment {
        NSS_WRAPPER_PASSWD = '/tmp/passwd'
        NSS_WRAPPER_GROUP = '/dev/null'
      }
      steps {
        script {
          sshagent (credentials: [ "collabora-rodoric-docsync", ] ) {
            sh 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}'
            sh 'LD_PRELOAD=libnss_wrapper.so rsync -e "ssh -oStrictHostKeyChecking=no" -va --no-group --no-owner --chmod=ugo=rwX --delete-after out/usr/share/doc/apertis-docs/apertis-docs/ docsync@developer.apertis.org:/srv/developer.apertis.org/www/${RELEASE}'
          }
        }
      }
    }

    stage("Build pdfs") {
      steps {
        sh "make pdf"
      }
    }

    stage ("Upload pdfs") {
      environment {
        NSS_WRAPPER_PASSWD = '/tmp/passwd'
        NSS_WRAPPER_GROUP = '/dev/null'
      }
      steps {
        script {
          sshagent (credentials: [ "collabora-rodoric-docsync", ] ) {
            sh 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}'
            sh 'LD_PRELOAD=libnss_wrapper.so rsync -e "ssh -oStrictHostKeyChecking=no" -va --no-group --no-owner --chmod=ugo=rwX --delete-after build/pdf/*.pdf docsync@developer.apertis.org:/srv/developer.apertis.org/www/${RELEASE}'
          }
        }
      }
    }

    stage ("Link latest") {
      when {
        expression {
          env.GIT_BRANCH == 'origin/master'
        }
      }
      environment {
        NSS_WRAPPER_PASSWD = '/tmp/passwd'
        NSS_WRAPPER_GROUP = '/dev/null'
      }
      steps {
        script {
          sshagent (credentials: [ "collabora-rodoric-docsync", ] ) {
            sh 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}'
            sh 'LD_PRELOAD=libnss_wrapper.so ssh -oStrictHostKeyChecking=no docsync@developer.apertis.org ln -sf --no-dereference ${RELEASE} /srv/developer.apertis.org/www/latest'
          }
        }
      }
    }
  }

  post {
    always {
      deleteDir()
    }
  }
}
